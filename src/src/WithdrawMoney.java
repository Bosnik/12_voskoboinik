
public class WithdrawMoney extends Thread {

    public WithdrawMoney() {
        start();
    }

    @Override
    public void run() {
        while (Bank.isBankomatWorking) {
            Bank.account.withdraw(500l);
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
