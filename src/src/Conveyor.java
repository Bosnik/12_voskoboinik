
public class Conveyor  {
    public static void main(String[] args) {
        Worker workerVasya = new Worker("Vasya", "Thread2");
        Worker workerPetya = new Worker("Petya", "Thread1");

        workerVasya.start();
        try {
            workerVasya.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        workerPetya.start();
    }
}
