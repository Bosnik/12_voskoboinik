import java.util.*;
import java.util.function.Consumer;

public class ThreadMultiply extends Thread {

    private int from;
    private int to;
    private static long[][] a;
    private static long[][] b;
    private static long[][] c;
    private static long executionTime;

    public ThreadMultiply(int from, int to) {
        this.from = from;
        this.to = to;
    }

    private void multiply(int from, int to) {
        for (int i = from; i <= to; i++) {
            for (int j = 0; j < c[0].length; j++) {
                int n = 0;
                for (int k = 0; k < b.length; k++) {
                    n += a[i][k] * b[k][j];
                }
                c[i][j] = n;
            }
        }
    }

    //Special consumer for joining threads without exception. Why? 'cause we can!
    public static Consumer threadJoin = (thread) -> {try {((Thread)thread).join();} catch (InterruptedException e) {}};

    //Multiply given matrix in several parallel Threads
    public static long[][] multiplyParallel(long[][] a, long[][] b, int threadsQty) {
        long startTime = System.currentTimeMillis();
        ThreadMultiply.a = a;
        ThreadMultiply.b = b;
        c = new long[a.length][b[0].length];
        int delta = c.length / threadsQty;
        List<Thread> threads = new ArrayList<>();

        for (int j = 0; j < c.length; j += delta) {
            int toIndex = (j + delta) > c.length ? c.length - 1 : j + delta - 1;
            Thread thread = new ThreadMultiply(j, toIndex);
            threads.add(thread);
            thread.start();
        }

        threads.stream().forEach(threadJoin);
        executionTime = System.currentTimeMillis() - startTime;
        return c;
    }

    @Override
    public void run() {
        multiply(from, to);
    }


    public static void main(String[] args) {

        List<Integer> threadsQts = Arrays.asList(1, 2, 4, 8, 16);
        Map<Integer, Long> result = new TreeMap<>();

        threadsQts.stream().forEach(threadsQty -> {
            long[][] a = MatrixHelper.fillMatrix(new long[800][800]);
            long[][] b = MatrixHelper.fillMatrix(new long[800][800]);
            multiplyParallel(a, b, threadsQty);
            result.put(threadsQty, executionTime);
        });

        result.forEach((key, value) -> {
            System.out.printf("Thread(s): %d - %dms%n", key, value);
        });

    }

}
