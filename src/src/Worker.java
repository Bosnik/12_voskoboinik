
public class Worker extends Thread{
    private String name;

    public Worker(String workerName, String threadName) {
        super(threadName);
        this.name = workerName;
    }

    @Override
    public void run() {
        System.out.printf("Worker %s started to work!%n", name);
        for (int i = 0; i < 1000 ; i++) {
            System.out.printf("Detail number %d is done by %s%n", i, name);
        }
    }
}
