import java.util.Random;

public class MatrixHelper {
    public static long[][] fillMatrix(long[][] matrix) {
        Random rnd = new Random();
        int maxValue = Integer.MAX_VALUE/matrix.length;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = rnd.nextInt(20);
            }
        }
        return matrix;
    }

    public static void printMatrix(long[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }
}
