public class McDonaldsKitchenWaitNotify {

    private BigMacMenu bigMacMenu = new BigMacMenu();

    public void createBigMacMenu() {

        Thread hamburger = new Thread(() -> {
            System.out.println("Hamburger cooking started.");
            synchronized (bigMacMenu) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                bigMacMenu.setHamburgerReady(true);
                bigMacMenu.notifyAll();
            }
            System.out.println("Hamburger ready!");
        });

        Thread fries = new Thread(() -> {
            System.out.println("Fries cooking started.");
            synchronized (bigMacMenu) {
                try {
                    if (!bigMacMenu.isHamburgerReady()) {
                        System.out.println("Waiting for hamburger...");
                        while (!bigMacMenu.isHamburgerReady())
                            bigMacMenu.wait();
                    }
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                bigMacMenu.setFriesReady(true);
                bigMacMenu.notifyAll();
            }
            System.out.println("Fries ready!");
        });

        Thread cola = new Thread(() -> {
            synchronized (bigMacMenu) {
                try {
                    System.out.println("Cola cooking started.");
                    if (!bigMacMenu.isFriesReady()) {
                        System.out.println("Waiting for fries...");
                        while (!bigMacMenu.isFriesReady())
                            bigMacMenu.wait();
                    }
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                bigMacMenu.setColaReady(true);
                bigMacMenu.notifyAll();
            }
            System.out.println("Cola ready!");
        });
        hamburger.start();
        fries.start();
        cola.start();

        try {
            cola.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Big mac menu is ready!");
    }

    public static void main(String[] args) {
        McDonaldsKitchenWaitNotify kitchen = new McDonaldsKitchenWaitNotify();
        kitchen.createBigMacMenu();
    }
}
