
public class McDonaldsKitchenJoin {

    private BigMacMenu bigMacMenu = new BigMacMenu();

    public void createBigMacMenuJoin() {

        Thread hamburger = new Thread(() -> {
            System.out.println("Hamburger cooking started.");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            bigMacMenu.setHamburgerReady(true);
            System.out.println("Hamburger ready!");
        });

        Thread fries = new Thread(() -> {
            System.out.println("Fries cooking started.");
            try {
                Thread.sleep(2000);
                if (!bigMacMenu.isHamburgerReady()) {
                    System.out.println("Waiting for hamburger...");
                    hamburger.join();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            bigMacMenu.setFriesReady(true);
            System.out.println("Fries ready!");
        });

        Thread cola = new Thread(() -> {
            System.out.println("Cola cooking started.");
            try {
                Thread.sleep(2000);
                if (!bigMacMenu.isFriesReady()) {
                    System.out.println("Waiting for fries...");
                    fries.join();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            bigMacMenu.setColaReady(true);
            System.out.println("Cola ready!");
        });

        hamburger.start();
        fries.start();
        cola.start();

        try {
            cola.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Big mac menu is ready!");

    }

    public static void main(String[] args) {
        McDonaldsKitchenJoin mcDonaldsKitchenJoin = new McDonaldsKitchenJoin();
        mcDonaldsKitchenJoin.createBigMacMenuJoin();
    }


}
